export class Room {


    private id: number;
    private code: string;
    private cost: number;
    private capacity: number;
    private disponiblity: Array<Date>

    constructor(id: number, code: string, cost: number, capacity: number, disponiblity: Array<Date>){
        this.id = id;
        this.code = code;
        this.cost = cost;
        this.capacity = capacity;
        this.disponiblity = disponiblity
    }
    

}
