import { Component, OnInit } from '@angular/core';
import { Room } from './model/room';
import { RoomService } from './service/room.service';

@Component({
  selector: 'app-icons',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {

  rooms: Array<Room>
  constructor(private service: RoomService) { }

  ngOnInit() {
    this.service.list().subscribe(data => {
      this.rooms = data;
    });
  }

  public delete(event: Event, id: number){
    console.log('delete ' + id);
  }

}
