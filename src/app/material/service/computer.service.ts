import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComputerService {

  constructor(private http: HttpClient) { }


  list(): Observable<any> {
    return this.http.get("http://localhost:9000/lafactory/computers/list");
  }
}
