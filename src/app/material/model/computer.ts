export class Computer {

    private id: number;
    private code: string;
    private cost: number;
    private disponiblity: Array<Date>;
    private ram: number;
    private rom: number;
    private processor: string;
    private purchaseDate: Date;

    constructor(id: number, code: string, cost: number, disponiblity: Array<Date>, ram: number, rom: number, processor: string,
        purchaseDate: Date){
        this.id = id;
        this.code = code;
        this.cost = cost;
        this.disponiblity = disponiblity;
        this.processor = processor;
        this.ram = ram;
        this.rom = rom;
        this.purchaseDate = purchaseDate;
    }
}
