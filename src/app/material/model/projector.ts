export class Projector {

    private id: number;
    private code: string;
    private cost: number;
    private disponiblity: Array<Date>

    constructor(id: number, code: string, cost: number, disponiblity: Array<Date>){
        this.id = id;
        this.code = code;
        this.cost = cost;
        this.disponiblity = disponiblity
    }
}
