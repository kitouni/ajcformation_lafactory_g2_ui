import { Component, OnInit } from '@angular/core';
import { ComputerService } from './service/computer.service';
import { ProjectorService } from './service/projector.service';
import { Computer } from './model/computer';
import { Projector } from './model/projector';

declare const google: any;

interface Marker {
lat: number;
lng: number;
label?: string;
draggable?: boolean;
}
@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent implements OnInit {

    computers: Array<Computer>;
    projectors: Array<Projector>;

    constructor(private computerService: ComputerService, private projectorService: ProjectorService) { }

    ngOnInit() {

      this.computerService.list().subscribe(data => {
        this.computers = data;
      });

      this.projectorService.list().subscribe(data => {
        this.projectors = data;
      });
    }

}
