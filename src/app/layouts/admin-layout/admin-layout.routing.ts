import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { RHComponent } from '../../rh/r-h.component';
import { SubjectComponent } from '../../subject/subject.component';
import { RoomComponent } from '../../room/room.component';
import { MaterialComponent } from '../../material/material.component';
import { PlanningComponent } from '../../planning/planning.component';
import { RoomAddComponent } from 'app/room/components/room-add/room-add.component';


export const AdminLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'room',
    //     component: RoomComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'planning',
    //         component: PlanningComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'material',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'subject',
    //         component: SubjectComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'rh',     component: RHComponent },
    { path: 'subject',     component: SubjectComponent },
    { path: 'room',          component: RoomComponent },
    { path: 'material',           component: MaterialComponent },
    { path: 'planning',  component: PlanningComponent },
    { path: 'room/add', component: RoomAddComponent}

];
