import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { RHComponent } from '../../rh/r-h.component';
import { SubjectComponent } from '../../subject/subject.component';
import { RoomComponent } from '../../room/room.component';
import { MaterialComponent } from '../../material/material.component';
import { PlanningComponent } from '../../planning/planning.component';

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule
} from '@angular/material';
import { RoomAddComponent } from 'app/room/components/room-add/room-add.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    RHComponent,
    SubjectComponent,
    RoomComponent,
    MaterialComponent,
    PlanningComponent,
    RoomAddComponent
  ]
})

export class AdminLayoutModule {}
